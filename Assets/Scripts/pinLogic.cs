﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pinLogic : MonoBehaviour {

    public Transform target;
    public float speed;

    public InputField posText; // will use the dropdown if that works
    public Dropdown mDropdown;
    public Text showPosText;

    public GameObject G, E, C;

    
    //List<GameObject[]> l = new List<GameObject[]>();
    // Use this for initialization
    void Start () {
        
    }

    // to set floors above the point to inactive
    // currently uses SetActiveRecursively(), a depreciated method
    // may no longer work in the future;
    void setFloorsActive() 
    {

        G.SetActiveRecursively(true);
        E.SetActiveRecursively(true);
        C.SetActiveRecursively(true);
        
        char[] loc;
        loc = posText.text.ToCharArray();

        char building = loc[0];
        char lv = loc[1];
        int lvInt = 0;
        int.TryParse(lv.ToString(), out lvInt);
        Debug.Log("Current Building - Level: " + building + lv);
        
        GameObject[] floors;
        for (int i = 5; i > lvInt; i--)
        {

            Debug.Log("It went here");
            floors = GameObject.FindGameObjectsWithTag("" + i);

            foreach (GameObject go in floors)
            {
                go.SetActive(false);
            }
        }
    }
    
    public void ChangePos() // 
    {
        /*
        string newPos = posText.text;
        */
        string newPos = mDropdown.options[mDropdown.value].text;
        target = GameObject.Find(newPos).transform;
        showPosText.text = newPos;
        //setFloorsActive();
    }

    // Update is called once per frame
    void Update () {

        float step = speed * Time.deltaTime;
        // Move our position a step closer to the target.
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);

        

    }
}
