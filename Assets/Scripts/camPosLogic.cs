﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camPosLogic : MonoBehaviour {
    
    public Transform target;
    public float speed;

    public float h, v;

    //public float fov; 

    // Use this for initialization
    void Start () {
		
	}
    

    // Update is called once per frame
    void Update() {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        /*
        if (v > 0)
        {
            fov -= 5f;
            Debug.Log("Zoomed In");
        }

        // If the wheel goes down, increment 5 to "zoomTo"
        else if (v < 0)
        {
            fov += 5f;
            Debug.Log("Zoomed Out");
        }
        */
        transform.position = target.position;
        transform.Rotate(Vector3.up * h * speed);
    }
}
