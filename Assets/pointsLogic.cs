﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class pointsLogic : MonoBehaviour {

    public Dropdown mDropdown;
    public List<GameObject> points;
    public List<string> pointNames;

	// Use this for initialization
	void Start () {
        RefreshOptions();
	}

    public void RefreshOptions()
    {
        Regex rx = new Regex(@"^\w\d\d\d\w$");
        Regex rx1 = new Regex(@"^\w\d\d\d$");

        points = GameObject.FindGameObjectsWithTag("G1").ToList<GameObject>();

        foreach (var obj in points)
        {
            if (rx.IsMatch(obj.name) || rx1.IsMatch(obj.name))
            {
                pointNames.Add(obj.name);
                Debug.Log(obj.name);
            }

        }
        /*
        foreach (GameObject p in points)
        {
            pointNames.Add(p.name);
        }
        */
        mDropdown.ClearOptions();
        mDropdown.AddOptions(pointNames);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
