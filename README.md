# README #

### Summary ###

* Quick summary
3D map of the Trafalgar Campus done in Unity 3D. Will be implemented some how on the front end of our capstone
* Version 0.8
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

Unity package is available for download under downloads, that file only contains the assets
Does not contain the changes Mykhailo has made, which is arranging the floors so that all floors can be seen rather than being stacked on top one another


### How do I get set up? ###


* Either clone the branch or create a new projet and use the unity package from the downloads
	If creating a new project, make sure you have android studio installed.

* Under build settings, set it so it builds and android apk
* Under Edit>Project Settings>Graphics > Always Include Shaders, Add a new element and add Particles/Alpha Blended Premultiply

### Contribution guidelines ###

* This is Unity, take advantage of the fact that you can adjust values during run time instead of having to rebuild it every time
* Try to keep it consistant
* Attatch global scripts to the GLOBAL game object for easy access
* Its C#

### Who do I talk to? ###

* Any one of the capstone members


### Extra ###
Let me know if i'm forgetting something